CREATE TABLE `event_description` (
  `evDescID` int(11) NOT NULL AUTO_INCREMENT,
  `evID` int(11) DEFAULT NULL,
  `evDesc` longtext DEFAULT NULL,
  PRIMARY KEY (`evDescID`),
  KEY `evdescevID_idx` (`evID`),
  CONSTRAINT `evdescevID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4