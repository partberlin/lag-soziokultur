CREATE TABLE `event_location` (
  `evID` int(11) NOT NULL,
  `locID` int(11) NOT NULL,
  PRIMARY KEY (`evID`,`locID`),
  KEY `event_location_locID_idx` (`locID`),
  CONSTRAINT `event_location_evID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `event_location_locID` FOREIGN KEY (`locID`) REFERENCES `location` (`locID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4