CREATE TABLE `event_offers` (
  `evID` int(11) NOT NULL,
  `offID` int(11) NOT NULL,
  PRIMARY KEY (`evID`,`offID`),
  KEY `event_offers_offID_idx` (`offID`),
  CONSTRAINT `event_offers_evID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `event_offers_offID` FOREIGN KEY (`offID`) REFERENCES `offers` (`offID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4