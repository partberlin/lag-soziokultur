CREATE TABLE `event_tags` (
  `evID` int(11) NOT NULL,
  `tagID` int(11) NOT NULL,
  PRIMARY KEY (`evID`,`tagID`),
  KEY `eventTagstagID_idx` (`tagID`),
  CONSTRAINT `eventTagsevID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `eventTagstagID` FOREIGN KEY (`tagID`) REFERENCES `tag` (`tagID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='event_tags'