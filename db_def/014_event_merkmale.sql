CREATE TABLE `event_merkmale` (
  `evID` int(11) NOT NULL,
  `merID` int(11) NOT NULL,
  PRIMARY KEY (`evID`,`merID`),
  KEY `eventmermerID_idx` (`merID`),
  CONSTRAINT `eventmerevID` FOREIGN KEY (`evID`) REFERENCES `event` (`evID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `eventmermerID` FOREIGN KEY (`merID`) REFERENCES `merkmale` (`merID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4