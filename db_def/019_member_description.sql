CREATE TABLE `member_description` (
  `meDescID` int(11) NOT NULL AUTO_INCREMENT,
  `meID` int(11) NOT NULL,
  `meDesc` longtext DEFAULT NULL,
  PRIMARY KEY (`meDescID`),
  KEY `meIDmeDesc_idx` (`meID`),
  CONSTRAINT `meIDmeDesc` FOREIGN KEY (`meID`) REFERENCES `member` (`meID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4