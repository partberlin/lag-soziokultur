<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\mariadb\clMariaDB;

class clLAGDescription extends clLAGAPIBase {


	const DescMember = 1;
	const DescEvent = 2;

	private array $APIData;
	private int $ID;
	public function __construct(int $ID, array $Data, int $Typ) {
		$this->APIData = $Data;
		$this->ID = $ID;
		$table = null;
		$prefix = null;
		switch ($Typ){
			case self::DescMember: $table = "member_description"; $prefix = "me"; break;
			case self::DescEvent: $table = "event_description"; $prefix = "ev"; break;
			default: error_log("unbekante Description Typ: $Typ"); return;
		}
		foreach ($Data as $Desc){
			clMariaDB::escapeStr($Desc);
			$SQL = 'insert into %1$s (%2$sID, %2$sDesc) values (%3$d, "%4$s")';
			$SQL = sprintf($SQL, $table, $prefix, $ID, $Desc);
			clMariaDB::query($SQL);
			//error_log($SQL);
		}
	}



}