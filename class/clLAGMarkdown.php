<?php

namespace part\lag\tools;

use http\Exception;

class clLAGMarkdown {
	static function markdown(string $text): string{
		$text = self::headline2($text);
		$text = self::headline1($text);
		$text = self::bold($text);
		$text = self::italic($text);
		$text = self::listItems($text);
		$text = self::newline($text);
		return $text;
	}


	private static function headline2(string $text): string {
		$matches1 = [];
		$matches2 = [];
		$match = "##";
		$match1 = "/\n##/";
		$match2 = "/\r?\n/";
		while (preg_match($match1, $text, $matches1, PREG_OFFSET_CAPTURE)) {
			if(preg_match($match2, $text, $matches2, PREG_OFFSET_CAPTURE, $matches1[0][1] + 1)){
				$head = substr($text, $matches1[0][1] + 1, $matches2[0][1] - $matches1[0][1] - strlen($matches2[0][0]));
			}else{ //Text Ende
				$head = substr($text, $matches1[0][1] + 1, strlen($text) - $matches1[0][1]);
			}
			//$replacement = preg_quote(substr($head, -(strlen($head) - strlen($match))));
			$replacement = substr($head, -(strlen($head) - strlen($match)));
			$head = preg_quote($head);
			$text = preg_replace("/$head/", "<span class='ui header h3'>$replacement</span>", $text, 1);
		}
		return $text;
	}

	private static function headline1(string $text): string {
		$matches1 = [];
		$matches2[0][1] = 0;
		$match = "#";
		$match1 = "/\n#/";
		$match2 = "/\r?\n/";
		while (preg_match($match1, $text, $matches1, PREG_OFFSET_CAPTURE, $matches2[0][1])) {
			if(self::isMaskMatch($text, $matches1[0][1])){
				$matches2[0][1] = $matches1[0][1] + 1;
				continue;
			}
			if(preg_match($match2, $text, $matches2, PREG_OFFSET_CAPTURE, $matches1[0][1] + 1)){
				$head = substr($text, $matches1[0][1] + 1, $matches2[0][1] - $matches1[0][1] - strlen($matches2[0][0]));
			}else{ //Text Ende
				$matches2[0][1] = strlen($text);
				$head = substr($text, $matches1[0][1] + 1, strlen($text) - $matches1[0][1]);
			}
			//$replacement = preg_quote(substr($head, -(strlen($head) - strlen($match))));
			$replacement = substr($head, -(strlen($head) - strlen($match)));
			$head = preg_quote($head);
			$text = preg_replace("/$head/", "<span class='ui header h3'>$replacement</span>", $text, 1);
		}
		return $text;
	}

	private static function listItems(string $text): string {
		$matches1 = [];
		$matches2[0][1] = 0;
		$match = "*";
		$match1 = "/\n\*/";
		$match2 = "/\r?\n/";
		while (preg_match($match1, $text, $matches1, PREG_OFFSET_CAPTURE, $matches2[0][1])) {
			if(self::isMaskMatch($text, $matches1[0][1])){
				$matches2[0][1] = $matches1[0][1] + 1;
				continue;
			}
			if(preg_match($match2, $text, $matches2, PREG_OFFSET_CAPTURE, $matches1[0][1] + 1)){
				$listItem = substr($text, $matches1[0][1] + 1, $matches2[0][1] - $matches1[0][1] - strlen($matches2[0][0]) + 1);
			}else{ //Text Ende
				$matches2[0][1] = strlen($text);
				$listItem = substr($text, $matches1[0][1] + 1, strlen($text) - $matches1[0][1] + 1);
			}
			//$replacement = preg_quote(substr($listItem, - (strlen($listItem) - strlen($match))));
			$replacement = substr($listItem, - (strlen($listItem) - strlen($match)));
			$listItem = preg_quote($listItem);

			$text = preg_replace("/$listItem/", "<span class='listitem'>$replacement</span>", $text, 1);
		}
		return $text ?? "";
	}

	private static function isMaskMatch(string &$text, int $match) : bool{
		if(substr($text,$match - 1,1) == '\\'){ // \\ ist \
			//cl("--->" . substr($text, 0, $match - 1));
			//cl("--->" . substr($text, $match));
			$text = substr($text, 0, $match - 1) . substr($text, $match);
			return true;
		}
		return false;
	}

	private static function bold(string $text): string {
		$matches1 = [];
		$matches2 = [];
		$match = "**";
		$match1 = "/\*\*/";
		$match2 = $match1;
		while (preg_match($match1, $text, $matches1, PREG_OFFSET_CAPTURE)) {
			if(preg_match($match2, $text, $matches2, PREG_OFFSET_CAPTURE, $matches1[0][1] + 1)){
				$bold = substr($text, $matches1[0][1], $matches2[0][1] - $matches1[0][1] + strlen($match));
			}else{ //Text Ende
				$bold = substr($text, $matches1[0][1], strlen($text) - $matches1[0][1]);
			}
			$replacement = preg_quote(substr($bold, strlen($match),strlen($bold) - strlen($match) * 2));
			$bold = preg_quote($bold);
			$text = preg_replace("/$bold/", "<b>$replacement</b>", $text, 1);
		}
		return $text;
	}

	private static function italic(string $text): string {
		$matches1 = [];
		$matches2 = [];
		$match = "__";
		$match1 = "/__/";
		$match2 = $match1;
		while (preg_match($match1, $text, $matches1, PREG_OFFSET_CAPTURE)) {
			if(preg_match($match2, $text, $matches2, PREG_OFFSET_CAPTURE, $matches1[0][1] + 1)){
				$italic = substr($text, $matches1[0][1], $matches2[0][1] - $matches1[0][1] + strlen($match));
			}else{ //Text Ende
				$italic = substr($text, $matches1[0][1], strlen($text) - $matches1[0][1]);
			}
			$replacement = preg_quote(substr($italic, strlen($match),strlen($italic) - strlen($match) * 2));
			$italic = preg_quote($italic);
			$text = preg_replace("/$italic/", "<i>$replacement</i>", $text, 1);
		}
		return $text;
	}

	private static function newline(string $text): string{
		$match = "/\r?\n\r?\n/";
		while (preg_match($match, $text)) {
			$text = preg_replace($match, '<br><br>', $text);
		}
		return $text;
	}


}