<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\lag\tools\clLAGMarkdown;
use part\mariadb\clMariaDB;
use PHPMailer\PHPMailer\Exception;

class clLAGEvent extends clLAGAPIBase {

	private object $APIData;
	private string $IDorg;
	private int $IDMember;
	private array $saveFields = [
		"meIDOrg" => "meIDOrg",
		"evName" => "name",
		"evStartDate" => "startDate",
		"evEndDate" => "endDate",
		"evLanguage" => "@language",
		"evAttendanceMode" => "eventAttendanceMode",
		"evStatus" => "eventStatus",
		"evPreStartDate" => "previousStartDate",
		"evDateModified" => "dateModified",
		"evDuration" => "duration",
		"evDoorTime" => "doorTime",
		"evParentID" => "evParentID",
		"evUrl" => "url",
		"evDescription" => "evDescription",
		"evIDOrg" => "evIDOrg",
	];
	public function __construct(object $Data) {
		$this->APIData = $Data;
		$this->IDorg = $this->getIdentifier("ID", $Data->organizer ?? new \stdClass())[0] ?? '';
		if(!$this->IDorg){
			print "keine Mitglieder ID gefunden\n";
			return;
		}
		$member = clLAGMember::getMember($this->IDorg);
		if(!isset($member->meIDorg)){
			error_log("Member ID not found, lege hidden Member an." . $this->IDorg);
			clLAGMember::createHiddenMember($this->IDorg);
			$member = clLAGMember::getMember($this->IDorg);
			//return;
		}
		$Data->evDescription = json_encode($Data);
		$Data->evIDOrg = $this->getIdentifier("SP-ID", $Data ?? new \stdClass())[0] ?? '';
		$Data->meIDOrg = $this->IDorg;
		$Data->evParentID = $this->getIdentifier("Parent-ID", $Data ?? new \stdClass())[0] ?? '';
		if($Data->startDate) {
			$Data->startDate = $this->convertDate($Data->startDate);
		}
		if(isset($Data->endDate)) {
			$Data->endDate = $this->convertDate($Data->endDate);
		}
		if(isset($Data->eventAttendanceMode)){
			$Data->eventAttendanceMode = str_replace("https://schema.org/", "", $Data->eventAttendanceMode);
			$Data->eventAttendanceMode = str_replace("EventAttendanceMode", "", $Data->eventAttendanceMode);
		}
		if(isset($Data->eventStatus)){
			$Data->eventStatus = str_replace("https://schema.org/Event", "", $Data->eventStatus);
			if($Data->eventStatus == "Cancelled"){ // abgesagte Events nicht Importieren
				return;
			}
		}
		if(isset($Data->previousStartDate)) {
			$Data->previousStartDate = $this->convertDate($Data->previousStartDate);
		}

		$Data->dateModified = $this->getIdentifier("dateModified", $Data ?? new \stdClass())[0] ?? null;
		if($Data->dateModified) {
			$Data->dateModified = $this->convertDate($Data->dateModified);
		}
		if(isset($Data->doorTime)) {
			$Data->doorTime = $this->convertDate($Data->doorTime);
		}


		$this->ID = clLAGDB::saveEvent($Data, $this->saveFields);
		if(isset($Data->offers)){
			new clLAGOffers($this->ID, $Data->offers);
		}
		clLAGDB::saveKategorien($this->ID, $this->getIdentifier("Kategorie", $Data), false);
		clLAGDB::saveKategorien($this->ID, $this->getIdentifier("Kategorie gemappt", $Data), true);
		clLAGDB::saveTagEvent($this->ID, $this->getIdentifier("Suchkriterien", $Data));
		clLAGDB::saveMerkmaleEvent($this->ID, $this->getIdentifier("Merkmale", $Data));

		if(isset($Data->description)) {
			new clLAGDescription($this->ID, is_array($Data->description) ? $Data->description : [$Data->description], clLAGDescription::DescEvent);
		}

		new clLAGImage($this->ID, $Data->image ?? [], clLAGImage::ImgEvent);
		if($Data->location){
			foreach ($Data->location as $loc){
				switch ($loc->{'@type'}){
					case "Place":
						if($loc->address) {
							if ($loc->name) {
								$loc->address->addName = $loc->name;
							}
							new clLAGAddress($this->ID, [$loc->address], clLAGAddress::AddrEvent);
							if(isset($loc->geo)) {
								new clLAGLocation($this->ID, [$loc->geo], clLAGLocation::LocEvent);
							}
						}
						break;
					default:
						//print "{$loc->{'@type'}}\n";
						new clLAGLocation($this->ID, [$loc], clLAGLocation::LocEvent);
				}
			}
		}


		$Data->evPDFs = $this->getIdentifier("PDF", $Data ?? new \stdClass(), "url");
		//$Data->evPDFs[]="http://icommit.part.berlin/wp-content/uploads/pdf-dokument-demo.pdf";
		if(count($Data->evPDFs) > 0) {
			$c = new \stdClass();
			$c->Type = 'PresseInfo';
			$c->Url = $Data->evPDFs[0];
			clLAGDB::saveLocationEvent($this->ID, $c, ["locType" => "Type", "locUrl" => "Url"]);
		}

	}







	static function searchEvents(\stdClass $parms): \stdClass{

		$ret = new \stdClass();
		$ret->categorys = [];
		$ret->locations = [];
		$ret->merkmale = [];
		$ret->events = [];
		$ret->dates = [];

		$SQL = "
		SELECT e.evIDOrg as evID, e.meIDOrg, e.evParentID, e.evName, c.catName, e.evStartDate, e.evEndDate, 
		       a.addOrt, date_format(e.evStartDate,'%Y-%m-%d') enableDates, ifnull(c.catID,0) catID, m.merName, 
		       ifnull(m.merID,0) merID, t.tagName, ifnull(t.tagID,0) tagID, ifnull(a.addLandkreis, '') addLandkreis,
		       i.imgCreator,i.imgCaption,i.imgUrlMD5, i.imgLicense, i.imgDescription FROM event e
			left join event_tags et on et.evID = e.evID
			left join event_categorie ec on ec.evID = e.evID
			left join event_merkmale em on em.evID = e.evID
			left join categorie c on c.catID = ec.catID
			left join merkmale m on m.merID = em.merID
			left join tag t on t.tagID = et.tagID
			left join event_address ea on ea.evID = e.evID    
			left join address a on a.addID = ea.addID
			left join (select min(evID) evID, imgID from event_images group by evID) ei on ei.evID = e.evID
			left join images i on i.imgID = ei.imgID
			where e.evStartDate >= date_format(now(), '%Y-%m-%d')
			";

		$where = [];
		if(isset($parms->tagID) && $parms->tagID > 0){
			$where[] = "t.tagID = {$parms->tagID}";
		}
		if(isset($parms->id_org) && strlen($parms->id_org) > 0){
			$where[] = "e.meIDorg = '{$parms->id_org}'";
		}
		if(isset($parms->categorys) && count($parms->categorys) > 0){
			$where[] = "c.catID in (" . implode(", ", $parms->categorys) . ")";
		}
		if(isset($parms->merkmale) && count($parms->merkmale) > 0){
			$where[] = "m.merID in (" . implode(", ", $parms->merkmale) . ")";
		}
		if(isset($parms->locations) && count($parms->locations) > 0){
			$where[] = "a.addLandkreis in ('" . implode("', '", $parms->locations) . "')";
		}
		if(isset($parms->datum) && strlen($parms->datum) == 10){
			$where[] = "date_format(e.evStartDate,'%d.%m.%Y') = '{$parms->datum}'";
		}

		if(count($where) > 0){
			$SQL .= " and " . implode(" and ", $where);
		}

		$SQL .= " order by e.evStartDate";
		$result = clMariaDB::queryObject($SQL) ?? [];
		$ret->events = [];
		$event = null;
		$evID = 0;
		foreach ($result as $row){
			if($row->merID > 0){
				$ret->merkmale[] = $row->merID;
			}
			if($row->catID > 0){
				$ret->categorys[] = $row->catID;
			}
			if(strlen($row->addLandkreis) > 0){
				$ret->locations[] = $row->addLandkreis;
			}
			if(strlen($row->enableDates) > 0){
				$ret->dates[] = $row->enableDates;
			}
			if($evID != $row->evID){
				if($event){
					$event->categorys = array_values(array_unique($event->categorys));
					$ret->events[] = $event;
				}
				$evID = $row->evID;
				$event = $row;
				$event->categorys = [$row->catName];
				//$event->img = clLAGImage::getImagePostID($row->imgUrlMD5);
				$event->img_html_tag = "";
				if(count($ret->events) < 6 && isset($row->imgUrlMD5)) {
					$event->img = clLAGImage::createImageObj($row);
					$event->img_html_tag = createImage($event->img);
				}else{
					$event->imgUrlMD5 = $row->imgUrlMD5;
				}
			}else{
				$event->categorys[] = $row->catName;
			}
		}
		if($evID > 0){
			$event->categorys = array_values(array_unique($event->categorys));
			$ret->events[] = $event;
		}
		//$ret->merkmale = array_values(array_intersect_key( $ret->merkmale , array_unique( array_map('serialize' , $ret->merkmale ) ) ));
		//$ret->categorys = array_values(array_intersect_key( $ret->categorys , array_unique( array_map('serialize' , $ret->categorys ) ) ));
		$ret->merkmale = array_values(array_unique($ret->merkmale));
		$ret->categorys = array_values(array_unique($ret->categorys));
		$ret->locations = array_values(array_unique($ret->locations));
		$ret->dates = array_values(array_unique($ret->dates));
		$ret->sql = $SQL;
		return $ret;
	}


	static function getEvent(int $evIDOrg) : ? \stdClass{
		$SQL = "
		SELECT e.evIDOrg as evID, e.meIDOrg, e.evParentID, e.evName, e.evStartDate, e.evEndDate, e.evUrl, e.evAttendanceMode, e.evDescription as evJSON,
		       ed.evDescID, ed.evDesc,
		       me.meName, me.meEmail, me.meUrl, me.meTelephone, ifnull(me.meShow, 0) meShow,
		       a.addName, a.addOrt, a.addPlZ, addStreet,
		       c.catName, o.offType, o.offUrl, o.offDescription, 
		       l.locID, l.locType, l.locName, l.locDescription, l.locUrl, 
		       date_format(e.evStartDate,'%Y-%m-%d') enableDates, ifnull(c.catID,0) catID, m.merName, ifnull(m.merID,0) merID, 
		       ifnull(a.addLandkreis, '') addLandkreis,i.imgCreator, i.imgCaption, i.imgDescription, i.imgUrl, i.imgUrlMD5, i.imgLicense  
			FROM event e
			left join event_categorie ec on ec.evID = e.evID
			left join event_merkmale em on em.evID = e.evID
			left join categorie c on c.catID = ec.catID
			left join merkmale m on m.merID = em.merID
			left join event_address ea on ea.evID = e.evID    
			left join address a on a.addID = ea.addID
			left join event_images ei on ei.evID = e.evID
			left join images i on i.imgID = ei.imgID
			left join event_offers eo on eo.evID = e.evID
			left join offers o on o.offID = eo.offID
			left join event_location el on el.evID = e.evID
			left join location l on l.locID = el.locID
			left join member me on me.meIDorg = e.meIDorg 
			left join event_description ed on ed.evID = e.evID
			where e.evIDOrg = $evIDOrg
			";
		$result = clMariaDB::queryObject($SQL) ?? [];
		$event = null;
		$imgs = [];
		if(count($result) > 0) {
			$event = $result[0];
			$event->merkmale = [];
			$event->categorys = [];
			$event->img = [];
			$event->location = [];
			$event->description = [];
			foreach ($result as $row) {
				if ($row->merID > 0) {
					$event->merkmale[] = $row->merName;
				}
				if ($row->catID > 0) {
					$event->categorys[] = $row->catName;
				}
				if($row->locID > 0){
					$c = new \stdClass();
					$c->Name = $row->locName;
					$c->Description = $row->locDescription;
					$c->Type = $row->locType;
					$c->Url = $row->locUrl;
					$c->ID = $row->locID;
					$event->location[$row->locType] = $c;
				}
				$event->description[$row->evDescID] = $row->evDesc;
				if (isset($row->imgUrlMD5) && !in_array($row->imgUrlMD5, $imgs)) {
					$imgs[] = $row->imgUrlMD5;
					$img = clLAGImage::getImagePostID($row->imgUrlMD5);
					if(gettype($img) === "string"){
						continue;
					}
					$img['imgCreator'] = $row->imgCreator;
					$img['imgCaption'] = $row->imgCaption;
					$img['imgDescription'] = $row->imgDescription;
					$img['imgUrl'] = $row->imgUrl;
					$event->img[] = $img;

				}
			}
			$event->description = array_values($event->description);
			foreach ($event->description as &$desc){
				$desc = clLAGMarkdown::markdown($desc ?? "");
			}
			$event->categorys = array_values(array_unique($event->categorys));
			$event->merkmale = array_values(array_unique($event->merkmale));
			$SQL = "select evStartDate, evEndDate, addOrt from event e 
					left join event_address ea on ea.evID = e.evID    
					left join address a on a.addID = ea.addID
				where evParentID = {$event->evParentID} order by evStartDate";
			$event->Termine = clMariaDB::queryObject($SQL) ?? [];
		}
		return $event;
	}

}