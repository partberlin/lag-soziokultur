<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;
use part\lag\tools\clLAGMarkdown;
use part\mariadb\clMariaDB;

class clLAGMember extends clLAGAPIBase {

	private object $APIData;
	private string $IDorg;
	private int $ID;
	private static $url_member = "/mitglieder/mitglied/";

	private array $saveFields = [
		"meEmail" => "email",
		"meUrl" => "url",
		"meTelephone" => "telephone",
		"meName" => "name",
		"meFoundingDate" => "foundingDate",
		"meIDorg" => "meIDorg",
		"meTeaser" => "meTeaser",
		"meDescription" => "meDescription",
	];
	public function __construct(object $Data) {
		$this->APIData = $Data;
		$Data->meDescription = json_encode($Data);
		$this->IDorg = $this->getIdentifier("ID", $Data)[0];
		$Data->meIDorg = $this->IDorg;
		if(isset($Data->disambiguatingDescription) && is_array($Data->disambiguatingDescription) && count($Data->disambiguatingDescription) > 0){
			$Data->meTeaser = $Data->disambiguatingDescription[0];
		}
		$this->ID = clLAGDB::saveMember($Data, $this->saveFields);
		clLAGDB::saveMerkmaleMember($this->ID, $this->getIdentifier("Merkmale", $Data));
		clLAGDB::saveTagMember($this->ID, $this->getIdentifier("Tag", $Data));
		new clLAGAddress($this->ID, $Data->address ?? [], clLAGAddress::AddrMember);
		new clLAGImage($this->ID, $Data->image ?? [], clLAGImage::ImgMember);
		new clLAGLocation($this->ID, $Data->location ?? [], clLAGLocation::LocMember);
		if(isset($Data->description)) {
			new clLAGDescription($this->ID, is_array($Data->description) ? $Data->description : [$Data->description], clLAGDescription::DescMember);
		}
	}

	static function getMember(string $meID) : \stdClass{
		$SQL = "select m.meIDorg, m.meName, m.meTeaser, m.meUrl, m.meTelephone, m.meEmail, m.meDescription as meJSON,
       			md.meDescID, md.meDesc,
				imgUrlMD5, imgCreator, imgCaption, imgDescription, imgLicense, imgUrl, tagName,
       			ifnull(av.addID,0) v_addID, av.addStreet v_addStreet, av.addPlZ v_addPlZ, av.addOrt v_addOrt, 
       			ifnull(ap.addID,0) p_addID, ap.addStreet p_addStreet, ap.addPlZ p_addPlZ, ap.addOrt p_addOrt 
				from member m
				left join member_images i on i.meID = m.meID
				left join images im on im.imgID = i.imgID
				left join member_address mav on mav.meID = m.meID 
				left join address av on av.addID = mav.addID and av.addType = 'visitor'    			
    			left join member_address map on map.meID = m.meID 
				left join address ap on ap.addID = map.addID and ap.addType = 'post'
				left join member_tags mt on mt.meID = m.meID
                left join tag t on t.tagID = mt.tagID
				left join member_description md on md.meID = m.meID
				where m.meIDorg = '$meID'";
		//error_log($SQL);
		$result = clMariaDB::queryObject($SQL) ?? [];

		$member = clMariaDB::queryObject($SQL)[0] ?? new \stdClass();

		$member->page_link = ["url" => self::$url_member . "?id=" . $meID];
		$member->link_button_text = null;
		$member->title = $member->meName ?? "";
		$member->headline = $member->meTeaser ?? "";
		$member->img = [];
		$member->tags = [];
		$member->description = [];
		$imgs = [];
		foreach ($result as $row) {
			if (isset($row->imgUrlMD5) && !in_array($row->imgUrlMD5, $imgs)) {
				$imgs[] = $row->imgUrlMD5;
				$img = clLAGImage::getImagePostID($row->imgUrlMD5);
				if(gettype($img) === "string"){
					continue;
				}
				$member->img[] = clLAGImage::createImageObj($row);
			}
			if(isset($row->tagName) && strlen(trim($row->tagName)) > 0) {
				$member->tags[] = $row->tagName;
			}
			$member->description[$row->meDescID] = $row->meDesc;
		}

		$member->description = array_values($member->description);
		foreach ($member->description as &$desc){
			$desc = clLAGMarkdown::markdown($desc ?? "");
		}
		$member->image = count($member->img) > 0 ? $member->img[0] : new \stdClass();
		$member->tags = array_unique($member->tags);
		return $member;
	}

	static function getMembers() : array{
		$members = [];

		$SQL = "select meIDorg, meTeaser,meName,imgUrlMD5, im.*, ifnull(av.addOrt, ap.addOrt) addOrt
				from member m
				left join (select meID, imgID from member_images group by meID) i on i.meID = m.meID
				left join (select meID, addOrt from member_address mav inner join address av on av.addID = mav.addID and av.addType = 'visitor') av on av.meID = m.meID
    			left join (select meID, addOrt from member_address mav inner join address av on av.addID = mav.addID and av.addType = 'post') ap on ap.meID = m.meID
				left join images im on im.imgID = i.imgID where meShow = 1";
		//error_log($SQL);
		$result = clMariaDB::queryObject($SQL) ?? [];
		foreach ($result as $row) {
			if (isset($row->imgUrlMD5)) {
				$row->image = clLAGImage::createImageObj($row);
			}
			$members[] = $row;
		}

		return $members;
	}

	static function createHiddenMember(string $IDOrg){
		$SQL = "insert into member (meIDorg, meShow) values ('$IDOrg', 0)";
		clMariaDB::query($SQL);
	}


	static function getLocations(){
		$SQL = "select * from member_location ml
			inner join member m on m.meID = ml.meID
			inner join location l on l.locID = ml.locID
			where locType = 'GeoCoordinates'";
		return clMariaDB::queryObject($SQL);
	}
}