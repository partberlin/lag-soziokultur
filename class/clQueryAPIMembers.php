<?php
declare(strict_types = 1);
namespace part\lag\api;

class clQueryAPIMembers extends clQueryAPI{
	function getData(clQueryAPICriteria $Criteria): array {
		$Criteria->{"search"} = "mitglieder";
		return parent::getData($Criteria);
	}
}