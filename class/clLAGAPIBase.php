<?php

namespace part\lag;

class clLAGAPIBase {

	protected function getIdentifier(string $Name, object $APIData, ? string $ValueField = "value") : array{
		$ident = [];
		if($APIData->identifier) {
			foreach ($APIData->identifier as $identifier) {
				if ($identifier->name == $Name) {
					$ident[] = trim($identifier->{$ValueField});
				}
			}
		}
		return $ident;
	}


	protected function convertDate(string $date) : string {
		if($clDate = \DateTime::createFromFormat(\DateTime::ISO8601, $date)){
			return $clDate->format("Y-m-d H:i:s");
		}
		error_log("Datumsformat nicht ISO8601");
		if($clDate = \DateTime::createFromFormat("Y-m-d", $date)){
			return $clDate->format("Y-m-d H:i:s");
		}
		error_log("Datumsformat nicht Y-m-d");
		return "0000-00-00";

	}
}