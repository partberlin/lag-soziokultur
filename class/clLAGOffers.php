<?php
declare(strict_types = 1);
namespace part\lag\api;
use part\lag\clLAGAPIBase;
use part\lag\db\clLAGDB;

class clLAGOffers extends clLAGAPIBase {
	private array $saveFields = [
		"offType" => "@type",
		"offUrl" => "url",
		"offDescription" => "description",
	];
	public function __construct(int $ID, object $Data) {
		clLAGDB::saveOffersEvent($ID, $Data, $this->saveFields);
	}









}